import hashlib
import logging
from scripts.handler.utility import Mongoutility

utility_object = Mongoutility()


class Mongodb:
    @staticmethod
    # got data from login_service
    def insert(data):
        register_details = utility_object.connection()
        # calling insert in utility file
        utility_object.insert(register_details, data)

    @staticmethod
    # checking for password
    def find_query(username, password):
        print(password)
        salt = "593p"
        db_password = password + salt
        h = hashlib.md5(db_password.encode())
        password1 = h.hexdigest()
        print(password1, "new")
        register_details = utility_object.connection()
        mycursor = utility_object.finding(register_details, {"User Id": username})
        flag1 = 0
        email1 = ""
        try:
            for key, value in mycursor.next().items():
                if key == "Mail id":
                    email1 = value
                if key == "Password" and value == password1:
                    flag1 = 1
                    break

        except Exception:
            # logging.exception("Exception occurred in handler_mycursor", exc_info=True)
            print("error occured")
        return flag1, email1

    @staticmethod
    # checking only user existence
    def find_queryuser(username):
        register_details = utility_object.connection()
        mycursor = utility_object.finding(register_details, {"User Id": username})
        flag = 0
        try:
            for key, value in mycursor.next().items():
                if key == "User Id" and value == username:
                    flag = 1
                    break
            return flag
        except Exception:
            print("error occured in handler_mycursor")
    # logging.exception("Exception occurred in handler_mycursor", exc_info=True)
