import logging
from pymongo import MongoClient


# connection
class Mongoutility:
    @staticmethod
    def connection():
        try:
            client = MongoClient('143.110.191.155', 37217)
            database = client.Pooja
            register_details = database.register_details
            return register_details
        except Exception:
            logging.exception("Exception occurred in connection", exc_info=True)
            print("exception occured in finding")

    @staticmethod
    def insert(register_details, data):
        try:
            # we can use insert_many for insertion all records at a time.
            register_details.insert(data)
        except Exception:
            logging.exception("Exception occurred", exc_info=True)
            print("exception occured in finding")

    @staticmethod
    def finding(register_details, query):
        try:
            details = register_details.find(query)
            return details
        except Exception:
            logging.exception("Exception occurred", exc_info=True)
            print("exception occured in finding")
