import logging
import hashlib
from flask import Blueprint, render_template, request
from scripts.handler.login_handler import Mongodb
from scripts.service.mailuser import mailing

blueprint_inserter = Blueprint('example_blueprint', __name__)
mongodb_object = Mongodb()
try:
    @blueprint_inserter.route('/login')
    def message():
        return render_template('login.html')


    @blueprint_inserter.route('/registration')
    def message1():
        return render_template('registration.html')


    @blueprint_inserter.route('/registration_success')
    def message2():
        return render_template('registration_success.html')


    @blueprint_inserter.route('/registration_data', methods=['POST'])
    def input_values():
        # encrypting the password using md5 algorithm using salt as 593p
        user_entered_password = request.form["password"]
        salt = "593p"
        db_password = user_entered_password + salt
        h = hashlib.md5(db_password.encode())
        # getting data from the form and assining to data variable
        data = {"Name": request.form["name"],
                "Phone number": request.form["number"],
                "Mail id": request.form["mail"],
                "User Id": request.form["user_id"],
                "Password": h.hexdigest()}
        resultuser = mongodb_object.find_queryuser(data["User Id"])
        if resultuser == 1:
            error = "user already exist"
            return render_template('login.html', error=error)
        mongodb_object.insert(data)
        return render_template('registration_success.html')


    @blueprint_inserter.route('/login_data', methods=['POST'])
    def required_values():
        result = mongodb_object.find_query(request.form["username"], request.form["pwd"])
        result1 = mongodb_object.find_queryuser(request.form["username"])
        if result[0] == 1:
            mailing(result[1], request.form["username"])

            return render_template('login_success.html')
        elif result1 == 1:
            error = "you have entered invalid password"
            return render_template('login.html', error=error)
        else:
            error = "user doesn't exist please register"
            return render_template('registration.html', error=error)
except Exception as e:
    logging.exception("Exception occurred in connection", exc_info=True)
    print(e)
